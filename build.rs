extern crate bindgen;
extern crate cc;

use std::env;
use std::path::PathBuf;

fn main() {
    cc::Build::new()
        .file("duktape/src/duktape.c")
        .include("duktape/src")
        .compile("duktape");
    
    let bindings = bindgen::Builder::default()
        .header("duktape/src/duktape.h")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("duktape_bindings.rs"))
        .expect("Couldn't write bindings!");
}